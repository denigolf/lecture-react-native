import React from 'react';
import { StatusBar } from 'expo-status-bar';
import ContactItem from '../ContactItem/ContactItem';

import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';

const ContactList = (props) => {
  const renderItem = ({ item }) => (
    <ContactItem
      item={item}
      navigation={props.navigation}
      contacts={props.contacts}
      setContacts={props.setContacts}
      memoryContacts={props.memoryContacts}
      setMemoryContacts={props.setMemoryContacts}
    />
  );
  return (
    <View style={styles.contactList}>
      <View style={{ flex: 1 }}>
        {true ? (
          <View
            style={{
              ...StyleSheet.absoluteFill,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <ActivityIndicator size="large" color="steelblue" />
          </View>
        ) : null}
        <FlatList
          data={props.contacts}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 50,
              }}
            >
              <Text style={{ color: '#fff' }}>No Contacts Found</Text>
            </View>
          )}
        />
        <StatusBar style="auto" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contactList: {
    flex: 1,
    borderWidth: 5,
    borderColor: '#306a9a',
    backgroundColor: 'steelblue',
    padding: 10,
    borderRadius: 20,
    marginVertical: 10,
  },
});

export default ContactList;
