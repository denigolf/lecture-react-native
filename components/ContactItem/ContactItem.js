import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const ContactItem = (props) => {
  const { item, contacts, setContacts, memoryContacts, setMemoryContacts } =
    props;

  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        minHeight: 70,
        padding: 5,
      }}
    >
      <View
        style={{
          ...styles.icon,
        }}
      >
        <Text style={styles.iconContent}>{item.name[0]}</Text>
      </View>
      <View>
        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 26 }}>
          {item.name}
        </Text>
        <Text style={{ color: 'white', fontWeight: 'bold' }}>
          {item.phoneNumbers[0].number}
        </Text>
      </View>
      <TouchableOpacity
        style={{ marginLeft: 'auto' }}
        onPress={() =>
          props.navigation.navigate('Contact', {
            item,
            contacts,
            setContacts,
            memoryContacts,
            setMemoryContacts,
          })
        }
      >
        <View style={styles.buttonWrapper}>
          <Text style={styles.buttonText}>i</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  iconContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 25,
    color: '#306a9a',
  },
  icon: {
    borderWidth: 5,
    borderColor: '#306a9a',
    borderRadius: 25,
    width: 50,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    padding: 1,
    backgroundColor: 'white',
  },
  buttonWrapper: {
    marginLeft: 'auto',
    width: 40,
    height: 40,
    borderRadius: 50,
    backgroundColor: '#306a9a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
  },
});

export default ContactItem;
