import React, { useState } from 'react';
import { TextInput, StyleSheet } from 'react-native';

const ContactItem = (props) => {
  const [searchValue, setSearchValue] = useState('');

  const searchContacts = (value) => {
    setSearchValue(value);
    props.setContacts(
      props.memoryContacts.filter((item) =>
        item.name.toLowerCase().includes(value.toLowerCase().trim())
      )
    );
  };

  return (
    <TextInput
      style={styles.searchInput}
      placeholderTextColor="#fff"
      placeholder="Search"
      value={searchValue}
      onChangeText={searchContacts}
    />
  );
};

const styles = StyleSheet.create({
  searchInput: {
    height: 50,
    backgroundColor: 'steelblue',
    borderWidth: 5,
    borderColor: '#306a9a',
    fontSize: 18,
    color: '#fff',
    padding: 10,
    paddingHorizontal: 20,
    width: '100%',
    borderRadius: 50,
  },
});

export default ContactItem;
