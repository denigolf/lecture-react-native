import React, { useState } from 'react';
import { View, StyleSheet, Button, TextInput, Alert } from 'react-native';
import * as Contacts from 'expo-contacts';

const AddContactPage = (props) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const { contacts, setContacts, setMemoryContacts } = props.route.params;

  const addContactHandler = async () => {
    if (firstName.trim() && lastName.trim() && phoneNumber.trim()) {
      const contact = {
        [Contacts.Fields.FirstName]: firstName,
        [Contacts.Fields.LastName]: lastName,
        [Contacts.Fields.PhoneNumbers]: [
          { label: 'mobile', number: phoneNumber },
        ],
      };

      const contactId = await Contacts.addContactAsync(contact);
      const createdContact = await Contacts.getContactByIdAsync(contactId);
      const newContacts = [...contacts, createdContact];
      setContacts(newContacts);
      setMemoryContacts(newContacts);
      props.navigation.goBack();
    } else {
      Alert.alert('Input error', 'Empty string is not valid value.');
    }
  };

  return (
    <View>
      <TextInput
        style={styles.textInput}
        placeholder="First name"
        value={firstName}
        onChangeText={(value) => setFirstName(value)}
      />
      <TextInput
        style={styles.textInput}
        placeholder="Last Name"
        value={lastName}
        onChangeText={(value) => setLastName(value)}
      />
      <TextInput
        style={styles.textInput}
        placeholder="Phone Number"
        value={phoneNumber}
        onChangeText={(value) => setPhoneNumber(value)}
      />
      <Button title="Add" onPress={addContactHandler} />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    padding: 20,
    borderBottomWidth: 2,
    marginBottom: 20,
  },
});

export default AddContactPage;
