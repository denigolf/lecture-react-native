import React, { useEffect, useState } from 'react';
import { StyleSheet, View, TouchableHighlight, Text } from 'react-native';
import * as Contacts from 'expo-contacts';
import SearchInput from '../../components/SearchInput/SearchInput';
import ContactList from '../../components/ContactList/ContactList';

const ContactsPage = (props) => {
  const [loading, setLoading] = useState(true);
  const [contacts, setContacts] = useState([]);
  const [memoryContacts, setMemoryContacts] = useState([]);

  const loadContacts = async () => {
    const permission = await Contacts.requestPermissionsAsync(
      Contacts.CONTACTS
    );

    if (permission.status !== 'granted') {
      return;
    }

    const { data } = await Contacts.getContactsAsync({
      fields: [Contacts.Fields.PhoneNumbers, Contacts.Fields.Image],
    });

    const filteredData = data.filter((item) =>
      item.name.includes('null') || !item.phoneNumbers ? false : item
    );

    setContacts(filteredData);
    setMemoryContacts(filteredData);
    setLoading(false);
  };

  useEffect(() => {
    setLoading(true);
    loadContacts();
  }, []);

  return (
    <View style={styles.container}>
      <SearchInput setContacts={setContacts} memoryContacts={memoryContacts} />
      <ContactList
        loading={loading}
        contacts={contacts}
        setContacts={setContacts}
        memoryContacts={memoryContacts}
        setMemoryContacts={setMemoryContacts}
        navigation={props.navigation}
      />
      <TouchableHighlight
        style={styles.submit}
        onPress={() =>
          props.navigation.navigate('Add Contact', {
            contacts,
            setContacts,
            memoryContacts,
            setMemoryContacts,
          })
        }
        underlayColor="#fff"
      >
        <Text style={styles.submitText}>+</Text>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  buttonWrapper: {
    borderRadius: 50,
  },
  submit: {
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'steelblue',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#fff',
  },
  submitText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default ContactsPage;
