import React from 'react';
import {
  Linking,
  Alert,
  Platform,
  Text,
  StyleSheet,
  View,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import * as Contacts from 'expo-contacts';

const ContactPage = (props) => {
  const { item, contacts, setContacts, setMemoryContacts } = props.route.params;
  const profileImage = item.image
    ? item.image.uri
    : 'https://p.kindpng.com/picc/s/78-785961_avatar-user-profile-image-png-transparent-png.png';
  const { name } = item;
  const phoneNumber = item.phoneNumbers[0].number;

  const callHandler = () => {
    let phone = phoneNumber;
    if (Platform.OS !== 'android') {
      phone = `telprompt:${phone}`;
    } else {
      phone = `tel:${phone}`;
    }
    Linking.canOpenURL(phone)
      .then((supported) => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phone);
        }
      })
      .catch((err) => console.log(err));
  };

  const deleteHandler = async () => {
    const contactId = item.id;
    await Contacts.removeContactAsync(contactId);
    const filteredContacts = contacts.filter(
      (contact) => contact.id != contactId
    );
    setContacts(filteredContacts);
    setMemoryContacts(filteredContacts);
    props.navigation.goBack();
  };

  return (
    <View style={styles.profileWrapper}>
      <Image
        style={styles.profileImg}
        source={{
          uri: profileImage,
        }}
      />
      <Text style={styles.nameText}>{name}</Text>
      <Text style={styles.contactText}>{phoneNumber}</Text>
      <View style={styles.buttons}>
        <TouchableOpacity style={styles.buttonWrapper}>
          <Button title="Call" color="steelblue" onPress={callHandler}></Button>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonWrapper}>
          <Button
            title="Delete"
            color="steelblue"
            onPress={deleteHandler}
          ></Button>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  profileWrapper: {
    marginTop: 20,
    paddingBottom: 20,
    display: 'flex',
    alignItems: 'center',
  },
  nameText: {
    marginTop: 20,
    fontSize: 25,
  },
  contactText: {
    marginTop: 5,
    fontSize: 15,
  },
  buttons: {
    width: '100%',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonWrapper: {
    flex: 1,
    marginTop: 10,
    margin: 10,
    padding: 10,
    width: '100%',
  },
  profileImg: {
    width: 200,
    height: 200,
    borderRadius: 200 / 2,
    overflow: 'hidden',
    borderWidth: 5,
    borderColor: '#306a9a',
  },
});

export default ContactPage;
