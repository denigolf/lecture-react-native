import React, { useEffect, useState } from 'react';
import { Button, StyleSheet, View } from 'react-native';
import * as Contacts from 'expo-contacts';
import SearchInput from './components/SearchInput/SearchInput';
import ContactList from './components/ContactList/ContactList';
import ContactsPage from './containers/ContactsPage/ContactsPage';
import ContactPage from './containers/ContactPage/ContactPage';
import AddContactPage from './containers/AddContactPage/AddContactPage';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

export default function App() {
  const Stack = createStackNavigator();
  // const [loading, setLoading] = useState(true);
  // const [contacts, setContacts] = useState([]);
  // const [memoryContacts, setMemoryContacts] = useState([]);

  // const loadContacts = async () => {
  //   const permission = await Contacts.requestPermissionsAsync(
  //     Contacts.CONTACTS
  //   );

  //   if (permission.status !== 'granted') {
  //     return;
  //   }

  //   const { data } = await Contacts.getContactsAsync({
  //     fields: [Contacts.Fields.PhoneNumbers, Contacts.Fields.Image],
  //   });

  //   const filteredData = data.filter((item) =>
  //     item.name.includes('null') || !item.phoneNumbers ? false : item
  //   );

  //   console.log(filteredData);
  //   setContacts(filteredData);
  //   setMemoryContacts(filteredData);
  //   setLoading(false);
  // };

  // useEffect(() => {
  //   setLoading(true);
  //   loadContacts();
  // }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Contacts" component={ContactsPage} />
        <Stack.Screen name="Contact" component={ContactPage} />
        <Stack.Screen name="Add Contact" component={AddContactPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
